import sbt._

object Dependencies {

  object V {
    val akka = "2.4.17"
    val twitter4s = "5.1"
    val deeplearning4j = "0.7.2"
    val json4s = "3.5.2"
    val opencsv = "2.4"
    val rabbitMQ = "2.8.1"
  }

  lazy val akka = Seq[ModuleID](
    "com.typesafe.akka" %% "akka-actor" % V.akka
  )

  lazy val twitter = Seq[ModuleID](
    "com.danielasfregola" %% "twitter4s" % V.twitter4s
  )

  lazy val deeplearning = Seq[ModuleID](
    "org.deeplearning4j" % "deeplearning4j-core" % V.deeplearning4j
  )

  lazy val json4s = Seq[ModuleID](
    "org.json4s" % "json4s-native_2.11" %  V.json4s
  )


  lazy val opencsv = Seq[ModuleID](
    "au.com.bytecode" % "opencsv" % V.opencsv
  )

  lazy val rabbitMQ = Seq[ModuleID](
    "com.rabbitmq" % "amqp-client" % V.rabbitMQ
  )

}