name := "TwitterParsing"

version := "1.0"

scalaVersion := "2.11.8"


javacOptions in Test += "Djava.library.path=./lib"
javacOptions in runMain += "Djava.library.path=./lib"


libraryDependencies ++= Dependencies.akka ++ Dependencies.twitter ++ Dependencies.deeplearning ++
  Dependencies.json4s ++ Dependencies.opencsv ++ Dependencies.rabbitMQ
