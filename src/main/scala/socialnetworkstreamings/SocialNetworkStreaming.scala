package socialnetworkstreamings

import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken}
import com.danielasfregola.twitter4s.entities.streaming.StreamingMessage


trait SocialNetworkStreaming {

  def createConnector: this.type

  def startStreaming(func: PartialFunction[StreamingMessage, Unit]): Unit

  def stopStreaming: Unit

  def name: String

  def consumerKey: ConsumerToken

  def accessKey: AccessToken

}
