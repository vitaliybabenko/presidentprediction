package socialnetworkstreamings

import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken}
import com.danielasfregola.twitter4s.entities.enums.Language
import com.danielasfregola.twitter4s.entities.streaming.StreamingMessage
import com.danielasfregola.twitter4s.http.clients.streaming.TwitterStream
import com.danielasfregola.twitter4s.{StreamingClients, TwitterStreamingClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

case class TwitterStreaming(_consumerToken: ConsumerToken, _accessToken: AccessToken, _name: String) extends SocialNetworkStreaming {
  var client = None: Option[StreamingClients]
  var streamingFuture = None: Option[TwitterStream]

  override def createConnector: this.type = {
    client = Some(TwitterStreamingClient(_consumerToken, _accessToken))
    this
  }

  override def startStreaming(func: PartialFunction[StreamingMessage, Unit]): Unit = {
    if (client.isEmpty) {
      createConnector
    }
    val result = Some(client.get.sampleStatuses(stall_warnings = true, languages = Seq(Language.English))(func))
    result.get.onComplete {
      case Success(data) =>
        println("successfully")
        streamingFuture = Some(data)
      case Failure(ex) => println("error while getting TwitterStream")
    }
  }

  override def stopStreaming: Unit = streamingFuture.get.close()

  override def name: String = _name
  def consumerKey:ConsumerToken = _consumerToken

  def accessKey:AccessToken = _accessToken


}
