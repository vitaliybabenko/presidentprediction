import actors.TwitterActorSystem
import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken}
import socialnetworkfilter.USAElectionFilter
import socialnetworkstreamings.TwitterStreaming

object TestTwitterArchitecture {
  def main(args: Array[String]): Unit = {
    import utils.Configurations._
    val users = List(("consumer1", "access1"), ("consumer2", "access2"), ("consumer3", "access3"))

    val twitterStreams: List[TwitterStreaming] = for (user <- users) yield {
      val consumerTk = consumerTokenKey(user._1)
      val consumerTokenScr = consumerTokenSecret(user._1)
      val accessTk = accessTokenKey(user._2)
      val accessTokenScr = accessTokenSecret(user._2)
      TwitterStreaming(ConsumerToken(consumerTk, consumerTokenScr), AccessToken(accessTk, accessTokenScr), user._2)
    }


    val twitterActorSystem = TwitterActorSystem

    val usaFilter = USAElectionFilter(("2016-11-09", "2015-08-30"))

    twitterActorSystem.init()

    twitterStreams.foreach(twitterActorSystem.startDataParsing(_, usaFilter))
  }
}
