package calculation

import actors.MLPrediction
import akka.actor.Actor

/** Stub implementation**/
case class CalculationActor() extends Actor {
  override def receive: Receive = {
    case MLPrediction(predictions) =>
      println(predictions)
  }
}




