package net

import com.rabbitmq.client.{Connection, ConnectionFactory}
import utils.Configurations

object RabbitMQConnection {
  private val connection = None: Option[Connection]

  /**
    * Return a connection if one doesn't exist. Else create
    * a new one
    */
  def getConnection: Connection = {
    connection match {
      case None => {
        val factory = new ConnectionFactory()
        factory.setHost(Configurations.rabbitMQHost)
        factory.setPort(Configurations.rabbitMQPort)
        factory.newConnection()
      }
      case Some(con) => con
    }
  }
}
