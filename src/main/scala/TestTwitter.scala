import java.io.FileWriter

import scala.io.Source

object TestTwitter extends App {

  //  val usaFilter = USAElectionFilter(("2016-11-09", "2015-08-30"))
  var clintoIter = 0
  var trumpIter = 0
  var both = 0
  val tweets = Source.fromFile("tweets.csv")

  val resultString: StringBuilder = StringBuilder.newBuilder

  val result = for (line <- tweets.getLines()) resultString.append(line + "\n")

  val splitted = resultString.toString().split(",\n")
  println(splitted.size)

  val writer = new FileWriter("formatted_tweets.csv", true)

  val trump = "trump,donald trump,trump donald,donaldtrump,trumpdonald,donald d.,trump d.".split(",")
  val clinton = "clinton,hillary clinton,clinton hillary,hillaryclinton,clintonhillary,hillary c.,clinton h.".split(",")

  splitted.foreach { item =>
    writer.append(item.replaceAll("\n+", "").replaceAll("@\\S+", "").trim + "," + getName(item.toLowerCase) + "\n")
  }

  writer.close()
  println(s"clinto: $clintoIter, trump: $trumpIter, both: $both")

  def getName(text: String): String = {
    var res = ""
    if (!trump.forall(!text.contains(_)) && !clinton.forall(!text.contains(_))) {
      both = both + 1
      res = "Both"
      return res
    }

    if (!trump.forall(!text.contains(_))) {
      trumpIter = trumpIter + 1
      res = "T"
      return res
    }

    if (!clinton.forall(!text.contains(_))) {
      clintoIter = clintoIter + 1
      res = "C"
      return res
    }
    res
  }
}
