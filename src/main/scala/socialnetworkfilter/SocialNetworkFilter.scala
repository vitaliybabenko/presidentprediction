package socialnetworkfilter

import com.danielasfregola.twitter4s.entities.Tweet
import com.danielasfregola.twitter4s.entities.enums.Language
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

trait SocialNetworkFilter[T] {

  protected val periodOfTime: (DateTime, DateTime)

  def countryFilter: T => Boolean

  def isRelatedContent: T => Boolean
}

trait ElectionFilter {
  protected val candidates: Seq[String]

  protected val countryShortNames: Seq[String]

  protected val citiesAndStates: Seq[String]
}

class USAElectionFilter(electionPeriod: (String, String)) extends SocialNetworkFilter[Tweet] with ElectionFilter {

  import utils.FileCVS._

  private[this] val yodaTimePattern = DateTimeFormat.forPattern("yyyy-MM-dd")
  override val periodOfTime: (DateTime, DateTime) = (yodaTimePattern.parseDateTime(electionPeriod._1), yodaTimePattern.parseDateTime(electionPeriod._2))
  override val candidates: Seq[String] = getDataFromFile("election_data.csv")
  override val countryShortNames: Seq[String] = getDataFromFile("usa_short_names.csv")
  override val citiesAndStates: Seq[String] = getDataFromFile("usa_cities_states.csv")

  import utils.TextFiltration._

  private[this] val wordOccurrences = findWord("(?<=^|\\s|#)", _: String, "(?=\\s|$)")

  def isUSACountry(location: Seq[String]): Boolean = !location.forall(!countryShortNames.contains(_))

  def isUSACities(location: Seq[String]): Boolean = !location.forall(!citiesAndStates.contains(_))

  def isRelatedContent: (Tweet) => Boolean = (tweet: Tweet) => {
    !candidates.forall(kw => wordOccurrences(kw).findFirstIn(tweet.text.toLowerCase).isEmpty) &&
      periodOfTime._1.isAfter(tweet.created_at.getTime) &&
      periodOfTime._2.isBefore(tweet.created_at.getTime)
  }

  override def countryFilter: Tweet => Boolean = (tweet: Tweet) => {
    val userLocation = tweet.user.get.location.map(_.toLowerCase)
    userLocation match {
      case Some(location) =>
        val filteredData = removalStopWords(wordFilter(location), Language.English)
        isUSACountry(filteredData) || isUSACities(filteredData)
      case None => false
    }
  }

}

object USAElectionFilter {
  def apply(electionPeriod: (String, String)): USAElectionFilter = new USAElectionFilter(electionPeriod)
}
















