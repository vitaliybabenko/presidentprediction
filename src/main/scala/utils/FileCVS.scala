package utils

import scala.io.Source

object FileCVS {
  def getDataFromFile(fileName: String): Seq[String] = {
    val file = Source.fromFile(fileName, enc = "UTF-8")
    val result = file.getLines().flatMap(_.split(",").map(_.trim)).toList
    file.close()
    result
  }
}
