package utils

import com.danielasfregola.twitter4s.entities.enums.Language
import com.danielasfregola.twitter4s.entities.enums.Language.Language

import scala.util.matching.Regex

object TextFiltration {

  val wordFilter = """[\p{Punct}]|[0-9]|\s+""".r

  def findWord(prefix: String, keyWord: String, suffix: String): Regex = s"$prefix$keyWord$suffix".r

  def wordFilter(text: String): String = wordFilter.replaceAllIn(text, ",").trim

  def removalStopWords(text: String, language: Language): Seq[String] = {
    val stopWords = StopWords.getStopWords(language)
    text.split(",").filter(!stopWords.contains(_))
  }
}

object StopWords {

  import FileCVS._

  private[this] val enStopWords = getDataFromFile("en_stop_words.csv")

  def getStopWords(language: Language): Seq[String] = {
    language match {
      case Language.English => enStopWords
      case _ => println("will be implemented in future"); Nil
    }
  }
}
