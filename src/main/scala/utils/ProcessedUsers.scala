package utils

import java.util.concurrent.{ConcurrentHashMap => ConcurrentHashSet}

object ProcessedUsers {
  private[this] val Object = new Object

  var processedUsers = new ConcurrentHashSet[String, Object]()
  val usersFromFile = FileCVS.getDataFromFile("processed_user.csv")

  usersFromFile.foreach(addUser)

  def addUser(string: String): Object = processedUsers.put(string, Object)
}
