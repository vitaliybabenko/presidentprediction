package utils

import com.danielasfregola.twitter4s.util.ConfigurationDetector
import com.typesafe.config.{Config, ConfigFactory}

object Configurations extends ConfigurationDetector {
  override def config: Config = ConfigFactory.load

  def consumerTokenKey(consumer: String): String = envVarOrConfig("TWITTER_CONSUMER_TOKEN_KEY", s"twitter.$consumer.key")

  def consumerTokenSecret(consumerSecret: String): String = envVarOrConfig("TWITTER_CONSUMER_TOKEN_KEY", s"twitter.$consumerSecret.secret")

  def accessTokenKey(access: String): String = envVarOrConfig("TWITTER_CONSUMER_TOKEN_KEY", s"twitter.$access.key")

  def accessTokenSecret(accessSecret: String): String = envVarOrConfig("TWITTER_CONSUMER_TOKEN_KEY", s"twitter.$accessSecret.secret")

  def rabbitMQHost: String = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.host")

  def rabbitMQPort: Int = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.port").toInt

  def rabbitMQQueuePredict: String = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.queue_prediction")

  def rabbitMQQueueResult: String = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.queue_result")

  def rabbitMQExchangePredict: String = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.exchange_prediction")

  def rabbitMQExchangeResult: String = envVarOrConfig("RABBIT_MQ_HOST", "rabbitmq.exchange_result")
}
