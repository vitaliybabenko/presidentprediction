package actors

import java.io.FileWriter

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, PoisonPill, Props}
import akka.routing.{DefaultResizer, FromConfig, SmallestMailboxPool}
import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken, Tweet}
import com.typesafe.config.ConfigFactory
import socialnetworkfilter.SocialNetworkFilter
import socialnetworksparsers.TwitterParser
import socialnetworkstreamings.SocialNetworkStreaming


trait SocialNetworkActorSystem[T] {
  def createStreamingActor(socialNetworkStreaming: SocialNetworkStreaming, filterCountry: Tweet => Boolean): ActorRef

  def createParserActor(filterFunction: Tweet => Boolean, writer: ActorRef): ActorRef

  def startDataParsing(socialNetworkStreaming: SocialNetworkStreaming, filter: SocialNetworkFilter[T]): Unit

  def stopDataParsing(): Unit

  def init(): Unit
}

object TwitterActorSystem extends SocialNetworkActorSystem[Tweet] {
  val actorSystem = ActorSystem.create("TwitterParsing", ConfigFactory.load().getConfig("TwitterParsing"))
  var streamingActors = List[ActorRef]()
  var parsingActors = List[ActorRef]()
  var killingActor = None: Option[ActorRef]
  var twitterWriter = None: Option[ActorRef]

  def createStreamingActor(socialNetworkStreaming: SocialNetworkStreaming, filterCountry: Tweet => Boolean): ActorRef = actorSystem.actorOf(Props(StreamingActor(socialNetworkStreaming, filterCountry)), socialNetworkStreaming.name)

  def createParserActor(filterFunction: Tweet => Boolean, writer: ActorRef): ActorRef = actorSystem.actorOf(SmallestMailboxPool(5, Some(DefaultResizer(lowerBound = 5, upperBound = 10))).
    props(Props(TweetsParserActor(filterFunction, writer))))

  def startDataParsing(socialNetworkStreaming: SocialNetworkStreaming, filter: SocialNetworkFilter[Tweet]): Unit = {
    val streamingActor = createStreamingActor(socialNetworkStreaming, filter.countryFilter)
    val parsingPool = createParserActor(filter.isRelatedContent, twitterWriter.get)
    val twitterParser = TwitterParser(socialNetworkStreaming.consumerKey, socialNetworkStreaming.accessKey)
    streamingActor ! StartStreamingMessage(parsingPool, twitterParser)
    killingActor.foreach(_ ! StartKillMessage(this))
    parsingActors = parsingActors :+ parsingPool
    streamingActors = streamingActors :+ streamingActor
  }

  def init(): Unit = {
    twitterWriter = Some(actorSystem.actorOf(FromConfig.props(Props[TweetsWriter]), "tweetsWriterPool"))
    killingActor = Some(actorSystem.actorOf(Props[KillingActor]))
  }

  def stopDataParsing(): Unit = {
    streamingActors.foreach(_ ! PoisonPill)
    parsingActors.foreach(_ ! PoisonPill)
    twitterWriter.foreach(_ ! PoisonPill)
  }
}

sealed case class StreamingActor(streamingClient: SocialNetworkStreaming, filterCountry: Tweet => Boolean) extends Actor with ActorLogging {
  val processedUsersWriter = new FileWriter("processed_user.csv", true)

  import utils.ProcessedUsers

  override def receive: Receive = {
    case StartStreamingMessage(parserActor: ActorRef, twitterParser: TwitterParser) =>
      log.info("start streaming")
      val socialNetworkStreaming = streamingClient.createConnector
      socialNetworkStreaming.startStreaming {
        case tweet: Tweet if tweet.user.get.location.isDefined =>
          val sizeBefore = ProcessedUsers.processedUsers.size
          if (filterCountry(tweet)) {
            ProcessedUsers.addUser(tweet.user.get.screen_name)
            if (ProcessedUsers.processedUsers.size != sizeBefore) {
              processedUsersWriter.append(s"${tweet.user.get.screen_name},")
              parserActor ! UserDataMessage(tweet.user.get.screen_name, twitterParser)
            }
          }
      }
  }

  override def postStop(): Unit = {
    log.info("stop streaming")
    streamingClient.stopStreaming
    processedUsersWriter.close()
  }
}

sealed case class TweetsWriter() extends Actor with ActorLogging {
  val tweetsWriter = new FileWriter("tweets.csv", true)

  override def receive: Receive = {
    case TweetsFromUserMessage(tweets) =>
      tweets.foreach(tw => tweetsWriter.append(s"${tw.text},\n"))
      println(s"tweets ${tweets.size}")
  }

  override def postStop(): Unit = {
    log.info("writer stop working")
    tweetsWriter.close()
  }
}

case class TweetsParserActor(filterFunction: Tweet => Boolean, writer: ActorRef) extends Actor with ActorLogging {
  override def receive: Receive = {
    case UserDataMessage(userScreenName, parser) =>
      val message = parser.getAllTweets(userScreenName, filterFunction)
      writer ! TweetsFromUserMessage(message)
  }

  override def postStop(): Unit = log.info("parser stop working")
}

//Todo dog-nail implementation, provide more accurate implementation in future
sealed case class KillingActor() extends Actor with ActorLogging {
  val stopMessage = "stop"

  override def receive: Receive = {
    case StartKillMessage(socialNetworkActorSystem) =>
      var console = Some("")
      while (!console.forall(_.equalsIgnoreCase(stopMessage))) {
        import scala.io.StdIn._
        console = Some(readLine())
      }
      log.info("start killing actors")
      socialNetworkActorSystem.stopDataParsing()
      context.stop(self)
  }
}

sealed case class StartStreamingMessage(parserActor: ActorRef, twitterParser: TwitterParser)

sealed case class StopMessage()

sealed case class UserDataMessage(userScreenName: String, twitterParser: TwitterParser)

sealed case class TweetsFromUserMessage(tweets: Seq[Tweet])

sealed case class StartParsingMessage()

sealed case class StartKillMessage(socialNetworkActorSystem: SocialNetworkActorSystem[Tweet])




