package actors

import akka.actor.{Actor, ActorRef, Props}
import com.rabbitmq.client.{Channel, QueueingConsumer}
import net.RabbitMQConnection
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.json4s.{DefaultFormats, _}

object RabbitMQService {

  val actorSystem = TwitterActorSystem.actorSystem
  var rabbitMQSender = None: Option[ActorRef]
  var rabbitMQReceiver = None: Option[ActorRef]
  // create the connection
  val connection = RabbitMQConnection.getConnection
  // create the channel we use to send


  // make sure the queue exists we want to send to

  def createSenderActor(queue: String, exchange: String, name: String = "rabbitMQSender"): ActorRef = {
    val sendingChannel = connection.createChannel()
    sendingChannel.queueDeclare(queue, false, false, false, null)
    rabbitMQSender = Some(actorSystem.actorOf(Props(RabbitMQSender(sendingChannel, queue, exchange)), name))
    rabbitMQSender.get
  }

  def createReceiverActor(queue: String, exchange: String, calculateActor: ActorRef, name: String = "rabbitMQReceiver"): ActorRef = {
    val receiverChannel = connection.createChannel()
    rabbitMQReceiver = Some(actorSystem.actorOf(Props(RabbitMQReceiver(receiverChannel, queue, calculateActor)), name))
    rabbitMQReceiver.get
  }
}

case class RabbitMQSender(channel: Channel, queue: String, exchange: String) extends Actor {
  def receive = {
    case Tweets(tweets) =>
      val msg = list2Json(tweets)
      channel.basicPublish(exchange, queue, null, msg.getBytes())
      println(s"rabbitMQSender send a message: size: ${msg.length}; head: ${msg}")
  }

  def list2Json(result: List[String]): String = {
    val json = "predictions" -> result.map(tweet => "tweet" -> tweet)
    compact(render(result))
  }
}

case class RabbitMQReceiver(channel: Channel, queue: String, calculateActor: ActorRef) extends Actor {
  def receive = {
    case StartListeningRabbitMQ => startReceiving()
  }

  def startReceiving() = {
    println("start receiving ")
    val consumer = new QueueingConsumer(channel)
    channel.basicConsume(queue, true, consumer)
    while (true) {
      val delivery = consumer.nextDelivery()
      val msg = new String(delivery.getBody)
      println(s"listener actor receive msg: $msg")
      try {
        val predictions = parseResult(msg)
        calculateActor ! MLPrediction(predictions)
      } catch {
        case e: Exception => println(s"receiver unknown message from rabbitMQ ${e.getCause}")
      }
    }
  }

  def validateResult(msg: String) = {
  }

  def parseResult(json: String): List[Int] = {
    implicit val formats = DefaultFormats
    parse(json).foldField(List[Int]())((acc, v) => acc ++ v._2.extract[List[Int]])
  }
}

case class StartListeningRabbitMQ()

case class MLPrediction(predictions: List[Int])

case class Tweets(tweets: List[String])






