import actors._
import akka.actor.{ActorSystem, Props}
import akka.stream.actor.ActorSubscriber
import calculation.CalculationActor
import utils.Configurations
import scala.concurrent.ExecutionContext.Implicits.global

import scala.io.Source

object TestRabbitMQ extends App {
  val actorSystem = TwitterActorSystem.actorSystem
  val queuePredictions = Configurations.rabbitMQQueuePredict
  val queueResult = Configurations.rabbitMQQueueResult
  val exchangePredictions = Configurations.rabbitMQExchangePredict
  val exchangeResult = Configurations.rabbitMQExchangeResult
  val calcActor = actorSystem.actorOf(Props[CalculationActor], "calcActor")
  val sender = RabbitMQService.createSenderActor(queuePredictions, exchangePredictions)
  val receiver = RabbitMQService.createReceiverActor(queuePredictions, exchangePredictions, calcActor)
  val file = Source.fromFile("formatted_tweets.csv")
  val list = file.getLines().take(10).toList

  receiver ! StartListeningRabbitMQ
//  Thread.sleep(1000)
  sender ! Tweets(list)

}
