package socialnetworksparsers

import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken}

trait SocialNetworkParser[T] {

  def getDataFromUser(userName: String, filterFunction: Option[T => T]): T
}
