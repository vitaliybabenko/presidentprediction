package socialnetworksparsers

import com.danielasfregola.twitter4s.TwitterRestClient
import com.danielasfregola.twitter4s.entities.{AccessToken, ConsumerToken, Tweet}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.concurrent.duration._

case class TwitterParser(consumerToken: ConsumerToken, accessToken: AccessToken) extends SocialNetworkParser[Seq[Tweet]] {
  var restClient = TwitterRestClient(consumerToken, accessToken)

  override def getDataFromUser(userName: String, filterFunction: Option[Seq[Tweet] => Seq[Tweet]]): Seq[Tweet] = getTweetsFromUser(None, userName)

  def getTweetsFromUser(oldId: Option[Long], userName: String): Seq[Tweet] = {
    val responseData = restClient.userTimelineForUser(max_id = oldId, screen_name = userName)
    Await.result(responseData, Duration.Inf).data
  }

  def filterTweets(f: Option[Seq[Tweet] => Seq[Tweet]], tweets: Seq[Tweet]): Seq[Tweet] = if (f.isDefined) f.get(tweets) else tweets

  def getAllTweets(userName: String, filterFunction: Tweet => Boolean): Seq[Tweet] = {
    val allTweets = ArrayBuffer[Tweet]()
    var newTweets = getTweetsFromUser(None, userName)
    while (newTweets.nonEmpty) {
      allTweets ++= newTweets.filter(filterFunction)
      newTweets = getTweetsFromUser(Some(newTweets.last.id - 1), userName)
    }
    allTweets
  }
}
