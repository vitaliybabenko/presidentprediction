import pandas as pd  # provide sql-like data manipulation tools. very handy.
from keras.layers import Dense, Embedding, LSTM
from keras.models import Sequential

pd.options.mode.chained_assignment = None
import numpy as np  # high dimensional vector computing library.
from copy import deepcopy
from string import punctuation
from random import shuffle

import gensim
from gensim.models.word2vec import Word2Vec  # the word2vec model gensim class

LabeledSentence = gensim.models.doc2vec.LabeledSentence  # we'll talk about this down below

from tqdm import tqdm

tqdm.pandas(desc="progress-bar")

from nltk.tokenize import TweetTokenizer  # a tweet tokenizer from nltk.

tokenizer = TweetTokenizer()

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer


def loadTrainData(filePath, labels):
    data = pd.read_csv(filePath, error_bad_lines=False)
    filterData(labels, data)
    data = data[data.Sentiment.isnull() == False]
    data['Sentiment'] = data['Sentiment'].map(int)
    data = data[data['SentimentText'].isnull() == False]
    data.reset_index(inplace=True)
    data.drop('index', axis=1, inplace=True)
    print 'dataset loaded with shape', data.shape
    return data


def filterData(labels, data):
    data.drop(labels, axis=1, inplace=True)


def loadTestData(filePath, labels):
    data = pd.read_csv(filePath, error_bad_lines=False)
    filterData(labels, data)
    data.reset_index(inplace=True)
    data.drop('index', axis=1, inplace=True)
    print 'dataset loaded with shape', data.shape
    return data


def tokenize(tweet):
    try:
        tweet = unicode(tweet.decode('utf-8').lower())
        tokens = tokenizer.tokenize(tweet)
        tokens = filter(lambda t: not t.startswith('@'), tokens)
        tokens = filter(lambda t: not t.startswith('#'), tokens)
        tokens = filter(lambda t: not t.startswith('http'), tokens)
        return tokens
    except:
        return 'NC'


def postprocess(data):
    data = data.head(data.size)
    data['tokens'] = data['SentimentText'].progress_map(
        tokenize)  ## progress_map is a variant of the map function plus a progress bar. Handy to monitor DataFrame creations.
    data = data[data.tokens != 'NC']
    data.reset_index(inplace=True)
    data.drop('index', inplace=True, axis=1)
    return data


def labelizeTweets(tweets, label_type):
    labelized = []
    for i, v in tqdm(enumerate(tweets)):
        label = '%s_%s' % (label_type, i)
        labelized.append(LabeledSentence(v, [label]))
    return labelized


def buildWordVector(tokens, size, tfidf, tweet_w2v):
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += tweet_w2v[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError:  # handling the case where the token is not
            # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count
    return vec


def trainWord2Vec(n_dim, x_train, frequency):
    tweet_w2v = Word2Vec(size=n_dim, min_count=frequency)
    tweet_w2v.build_vocab([x.words for x in tqdm(x_train)])
    tweet_w2v.train([x.words for x in tqdm(x_train)], total_examples=tweet_w2v.corpus_count, epochs=tweet_w2v.iter)
    return tweet_w2v


def buildTFIDFMatrix(x_train, min_df):
    print 'building tf-idf matrix ...'
    vectorizer = TfidfVectorizer(analyzer=lambda x: x, min_df=min_df)
    matrix = vectorizer.fit_transform([x.words for x in x_train])
    tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
    print 'vocab size :', len(tfidf)
    return tfidf


def getTextVector(n_dim, tfidf, tweet_w2v, x_train):
    from sklearn.preprocessing import scale

    train_vecs_w2v = np.concatenate(
        [buildWordVector(z, n_dim, tfidf, tweet_w2v) for z in tqdm(map(lambda x: x.words, x_train))])
    train_vecs_w2v = scale(train_vecs_w2v)
    return train_vecs_w2v


def createModel(n_dim):
    model = Sequential()
    model.add(Dense(32, activation='relu', input_dim=n_dim))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


# Maybe useful in future purposes
def saveModel():
    model.save_weights('tweets_weights')
    json_string = model.to_json()
    text_file = open("tweets_json", "w")
    text_file.write(json_string)
    text_file.close()


def trainModel(file, n_dim, epochs):
    data = loadTrainData(file, ['ItemID', 'SentimentSource'])

    data = postprocess(data)

    size = data.size

    x_train, x_test, y_train, y_test = train_test_split(np.array(data.head(size).tokens),
                                                        np.array(data.head(size).Sentiment), test_size=0.2,
                                                        stratify=np.array(data.head(size).Sentiment))
    x_train = labelizeTweets(x_train, 'TRAIN')

    x_test = labelizeTweets(x_test, 'TEST')

    tweet_w2v = trainWord2Vec(n_dim, x_train, 10)

    tfidf = buildTFIDFMatrix(x_train,10)

    train_vecs_w2v = getTextVector(n_dim, tfidf, tweet_w2v, x_train)

    test_vecs_w2v = getTextVector(n_dim, tfidf, tweet_w2v, x_test)

    # gives 79% accuracy
    model = createModel(n_dim)
    model.fit(train_vecs_w2v, y_train, epochs=epochs, batch_size=32)
    score = model.evaluate(test_vecs_w2v, y_test, batch_size=128)
    print "model accuracy: ", score[1]
    print "done successfully"
    return model


def predictValues(filePath, n_dim, model):
    data = loadTestData(filePath, ['ItemID', 'SentimentSource', 'Sentiment'])
    data = postprocess(data)
    x_test = labelizeTweets(data.head(data.size).tokens, "TEST")
    print x_test[0]
    print x_test[1]

    tweet_w2v = trainWord2Vec(n_dim, x_test, 1)
    tfidf = buildTFIDFMatrix(x_test,1)
    test_vecs_w2v = getTextVector(n_dim, tfidf, tweet_w2v, x_test)
    prediction = model.predict_classes(test_vecs_w2v)
    return prediction

model = trainModel('training2.csv', 200, 15)


prediction = predictValues('formatted_tweets.csv', 200, model)

print prediction
# Another neural network model, in future try to implement it, can gives better accuracy

# max_features = 20000
# batch_size = 32
#
# print('Build model...')
# model = Sequential()
# model.add(Embedding(max_features, 200))
# model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
# model.add(Dense(1, activation='sigmoid'))
#
# # try using different optimizers and different optimizer configs
# model.compile(loss='binary_crossentropy',
#               optimizer='adam',
#               metrics=['accuracy'])
#
# print('Train...')
# model.fit(train_vecs_w2v, y_train,
#           batch_size=batch_size,
#           epochs=15,
#           validation_data=(x_test, y_test))
# score, acc = model.evaluate(x_test, y_test,
#                             batch_size=batch_size)
# print('Test score:', score)
# print('Test accuracy:', acc)
